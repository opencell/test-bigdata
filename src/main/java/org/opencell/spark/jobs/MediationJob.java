package org.opencell.spark.jobs;

import java.util.Properties;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.opencell.spark.model.CDR;
import org.apache.spark.sql.api.java.UDF3;
import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;
import scala.Tuple3;

import java.sql.Date;

import scala.Function1;
import scala.Tuple2;
//import org.apache.spark.sql.SparkSession.implicits$;

public class MediationJob {
	
	public static final String CDR_ORIGIN_SPARK = "SPARK";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SparkSession spark = SparkSession
			      .builder()
			      .appName("APP 1")
			      .master("local[*]")
			      .getOrCreate();
	    
		StructType cdrCSVSchema = new StructType()
	    		.add("timestamp", DataTypes.TimestampType)
	    		.add("quantity", DataTypes.DoubleType)
	    		.add("access", DataTypes.StringType)
	    		.add("param1", DataTypes.StringType)
	    		.add("param2", DataTypes.StringType)
	    		.add("param3", DataTypes.StringType)
	    		.add("param4", DataTypes.StringType)
	    		.add("param5", DataTypes.StringType)
	    		.add("param6", DataTypes.StringType)
	    		.add("param7", DataTypes.StringType)
	    		.add("param8", DataTypes.StringType)
	    		.add("param9", DataTypes.StringType)
	    		.add("dateParam1", DataTypes.TimestampType)
	    		.add("dateParam2", DataTypes.TimestampType)
	    		.add("dateParam3", DataTypes.TimestampType)
	    		.add("dateParam4", DataTypes.TimestampType)
	    		.add("dateParam5", DataTypes.TimestampType)
	    		.add("decimalParam1", DataTypes.DoubleType)
	    		.add("decimalParam2", DataTypes.DoubleType)
	    		.add("decimalParam3", DataTypes.DoubleType)
	    		.add("decimalParam4", DataTypes.DoubleType)
	    		.add("decimalParam5", DataTypes.DoubleType)
	    		.add("extraParam", DataTypes.StringType)
	    		;
		
	    Dataset<CDR> cdr = spark
	    		.read()
	    		.format("csv")
	    		.option("header", "true")
	    		//.option("inferSchema", "true")
	    		.option("delimiter", ";")
	    		.option("dateFormat", "yyyy-MM-dd'T'hh:mm:ss.S'Z'")
	    		.schema(cdrCSVSchema)
	    		.csv("CDR_SAMPLE.csv")
	    		.as(Encoders.bean(CDR.class));
	    
	    long v = cdr.filter(x -> (x.timestamp != null && x.getAccess().length()>0)).count();
	    
	    System.out.println("validated entries :" + v);
	}

}
