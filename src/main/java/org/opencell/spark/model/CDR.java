package org.opencell.spark.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class CDR implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2403679111518361139L;
	
	public Date timestamp;
	public BigDecimal quantity;
	public String access;
	public String param1;
	public String param2;
	public String param3;
	public String param4;
	private String param5;
	private String param6;
	private String param7;
	private String param8;
	private String param9;
	private Date dateParam1;
	private Date dateParam2;
	private Date dateParam3; 
	private Date dateParam4; 
	private Date dateParam5; 
	private BigDecimal decimalParam1; 
	private BigDecimal decimalParam2; 
	private BigDecimal decimalParam3; 
	private BigDecimal decimalParam4; 
	private BigDecimal decimalParam5;
	private String extraParam;
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public String getParam1() {
		return param1;
	}
	public void setParam1(String param1) {
		this.param1 = param1;
	}
	public String getParam2() {
		return param2;
	}
	public void setParam2(String param2) {
		this.param2 = param2;
	}
	public String getParam3() {
		return param3;
	}
	public void setParam3(String param3) {
		this.param3 = param3;
	}
	public String getParam4() {
		return param4;
	}
	public void setParam4(String param4) {
		this.param4 = param4;
	}
	public String getParam5() {
		return param5;
	}
	public void setParam5(String param5) {
		this.param5 = param5;
	}
	public String getParam6() {
		return param6;
	}
	public void setParam6(String param6) {
		this.param6 = param6;
	}
	public String getParam7() {
		return param7;
	}
	public void setParam7(String param7) {
		this.param7 = param7;
	}
	public String getParam8() {
		return param8;
	}
	public void setParam8(String param8) {
		this.param8 = param8;
	}
	public String getParam9() {
		return param9;
	}
	public void setParam9(String param9) {
		this.param9 = param9;
	}
	public Date getDateParam1() {
		return dateParam1;
	}
	public void setDateParam1(Date dateParam1) {
		this.dateParam1 = dateParam1;
	}
	public Date getDateParam2() {
		return dateParam2;
	}
	public void setDateParam2(Date dateParam2) {
		this.dateParam2 = dateParam2;
	}
	public Date getDateParam3() {
		return dateParam3;
	}
	public void setDateParam3(Date dateParam3) {
		this.dateParam3 = dateParam3;
	}
	public Date getDateParam4() {
		return dateParam4;
	}
	public void setDateParam4(Date dateParam4) {
		this.dateParam4 = dateParam4;
	}
	public Date getDateParam5() {
		return dateParam5;
	}
	public void setDateParam5(Date dateParam5) {
		this.dateParam5 = dateParam5;
	}
	public BigDecimal getDecimalParam1() {
		return decimalParam1;
	}
	public void setDecimalParam1(BigDecimal decimalParam1) {
		this.decimalParam1 = decimalParam1;
	}
	public BigDecimal getDecimalParam2() {
		return decimalParam2;
	}
	public void setDecimalParam2(BigDecimal decimalParam2) {
		this.decimalParam2 = decimalParam2;
	}
	public BigDecimal getDecimalParam3() {
		return decimalParam3;
	}
	public void setDecimalParam3(BigDecimal decimalParam3) {
		this.decimalParam3 = decimalParam3;
	}
	public BigDecimal getDecimalParam4() {
		return decimalParam4;
	}
	public void setDecimalParam4(BigDecimal decimalParam4) {
		this.decimalParam4 = decimalParam4;
	}
	public BigDecimal getDecimalParam5() {
		return decimalParam5;
	}
	public void setDecimalParam5(BigDecimal decimalParam5) {
		this.decimalParam5 = decimalParam5;
	}
	public String getExtraParam() {
		return extraParam;
	}
	public void setExtraParam(String extraParam) {
		this.extraParam = extraParam;
	}	
}
